import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdMenuModule,
         MdToolbarModule,
         MdIconModule, 
         MdSidenavModule, 
         MdCardModule,
         MdInputModule, 
         MdButtonModule, 
         MdCheckboxModule,
         MdSelectModule ,
         MdSnackBarModule,
         MdGridListModule,
         MdProgressBarModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    MdSidenavModule,
    MdInputModule,
    MdButtonModule,
    MdCheckboxModule,
    MdMenuModule,
    MdCardModule,
    MdToolbarModule,
    MdIconModule,
    MdSelectModule,
    MdSnackBarModule,
    MdGridListModule,
    MdProgressBarModule,
    FlexLayoutModule
  ],
  exports: [
    BrowserAnimationsModule,
    MdSidenavModule,
    MdInputModule,
    MdButtonModule,
    MdCheckboxModule,
    MdMenuModule,
    MdCardModule,
    MdToolbarModule,
    MdIconModule,
    MdSelectModule,
    MdSnackBarModule,
    MdGridListModule,
    MdProgressBarModule,
    FlexLayoutModule
  ]
})
export class MaterialModule { }
