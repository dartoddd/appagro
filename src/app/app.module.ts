import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';

import {  MaterialModule } from './material.module';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component';
import { LogonComponent } from './login/login.component';
import { ProdutoAddComponent } from './produto-add/produto-add.component';
import { ProdutosComponent } from './produtos/produtos.component';

import { AuthenticateService } from './login/authenticate.service';
import { ProdutoAddService } from './produto-add/produto-add.service';
import { ProdutosService } from './produtos/produtos.service';
import { LoaderService } from './loader.service';
import { FreteService } from './produtos/frete.service'


@NgModule({
  imports: [
    BrowserModule,
    MaterialModule,
    AppRoutingModule,
    HttpModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    LogonComponent,
    ProdutoAddComponent,
    ProdutosComponent
  ],
  providers: [AuthenticateService,
    ProdutoAddService,
    ProdutosService,
    LoaderService,
    FreteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
