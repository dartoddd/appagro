import { Component, OnInit } from '@angular/core';
import { LoaderService } from './loader.service';

import { Subscription } from "rxjs/Subscription";
import { MediaChange, ObservableMedia } from '@angular/flex-layout';


const PRINT_MOBILE = 'print and (max-width: 600px)';

@Component({
  selector: 'app-agro',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  showLoader: boolean;

  watcher: Subscription;
  activeMediaQuery;


  constructor(
      private loaderService: LoaderService,
      private media:ObservableMedia) {

        //this.watcher = this.media
        //.subscribe((e: MediaChange) => {
        //      this.activeMediaQuery = e;
        //  });
  }


  ngOnInit() {
    this.loaderService.status.subscribe((val: boolean) => {
        this.showLoader = val;
    });
  }

  isSmallScreen()
  { return this.media.isActive('xs') || this.media.isActive('sm') }

  mode(){
    return this.isSmallScreen() ? 'over' : 'side';
  }

  token(){
    return localStorage.getItem('user-token')
  }

  limpaToken(){
    localStorage.removeItem('user-token')
  }
}
