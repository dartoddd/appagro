import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ProdutosService } from './produtos.service'
import { FreteService } from './frete.service'
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import { AuthenticateService } from '../login/authenticate.service';
import { Produto } from '../produtos/produto'
import { LoaderService } from '../loader.service';
import { FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit, AfterViewInit {

  produtos : Produto[] = [];
  cep : String;

  constructor( public authenticate:AuthenticateService,
               private produtosService: ProdutosService,
               private freteService: FreteService,
               private router: Router,
               public snackBar: MdSnackBar,
               private loaderService: LoaderService ) { }

  ngOnInit() {
    this.getAll();
  }

  ngAfterViewInit() {
  }

  getAll(){
    this.produtosService.get()
                          .then(resp => this.sucesso(resp))
                          .catch(error => this.erro(error._body));
    this.loaderService.display(true);
  }

  sucesso(resp){
    this.produtos = JSON.parse(resp._body);
    this.produtos.forEach(produto => { produto['imagem'] = this.image() })
    this.loaderService.display(false);
  }

  erro(erro){
    this.openSnackBar("ERRO" + erro, "Fechar")
    this.loaderService.display(false);
  }

  confirmarDelete(produto_id){
    let snackBarRef = this.snackBar.open("Excluir o produto?", "Sim", {duration: 3000})
    snackBarRef.onAction().subscribe(() => {
      this.delete(produto_id)
    });
    snackBarRef.afterDismissed().subscribe(() => {
      console.log('The snack-bar was dismissed');
    });
  }

  delete(produto_id){
    if(!this.authenticate.token()){
      return this.openSnackBar("Precisa estar logado", "Fechar")
    }
    this.loaderService.display(true);
    this.produtosService.delete(produto_id)
                          .then(resp => this.sucessoDelete(resp))
                          .catch(error => this.erro(error._body));
  }

  sucessoDelete(resp){
    this.loaderService.display(false);
    this.openSnackBar("Excluido com sucesso", "Fechar")
    this.getAll();
  }

  image(){
    return 'assets/images/'+ Math.floor((Math.random()*7)+1) + '.jpg'
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 0,
    });
  }

  buscaFrete(produto_id){
    
    let produto = this.produtos.filter( task => task.id === produto_id );
    let cep = String(produto[0].cep)
    
    if(cep.length == 8){
      this.freteService.get(cep, produto_id)
                          .then(resp => this.sucessoFrete(produto_id, resp._body))
                          .catch(error => console.info(error._body));
      this.loaderService.display(true);
    }
  }

  sucessoFrete(produto_id, body){
    let fretes = JSON.parse(body);

    this.produtos.forEach(p => { 
      if(p.id == produto_id){
          p.sedex = fretes.sedex
          p.pac = fretes.pac
        }
    });

    this.loaderService.display(false);
  }
}
