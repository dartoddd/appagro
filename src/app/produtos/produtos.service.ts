import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Produto } from './produto'
import { Constants } from '../constants';

@Injectable()
export class ProdutosService {

  constructor(private http: Http) { }

  get(): Promise<any>{
    console.info("GET - " + Constants.BASE_URL + 'produtos')

    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http.get(Constants.BASE_URL + 'produtos', { headers: headers } )
                    .toPromise()
                    .then(res => res )
                    .catch(this.handleError);
  }

  delete(produto_id): Promise<any>{
    console.info("DELETE - " + Constants.BASE_URL + 'produto')

    let headers = new Headers({'Content-Type': 'application/json', 
                               'Authorization': this.token()});

    return this.http.delete(Constants.BASE_URL + 'produtos/'+produto_id, { headers: headers } )
                    .toPromise()
                    .then(res => res )
                    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Erro na comunicação com o serviço de adicionar produto', error);
    return Promise.reject(error.message || error);
  }

  token(){
    return localStorage.getItem('user-token');
  }
}
