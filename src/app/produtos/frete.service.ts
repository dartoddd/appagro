import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Constants } from '../constants';

@Injectable()
export class FreteService {

  constructor(private http: Http) { }

  get(cep, produto_id): Promise<any>{
    let params = "produto_id=" +produto_id+ "&cep_destino=" + cep;
    console.info("GET - " + Constants.BASE_URL + 'frete?'+ params)

    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http.get(Constants.BASE_URL + 'frete?'+ params, { headers: headers} )
                    .toPromise()
                    .then(res => res )
                    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Erro na comunicação com o serviço de adicionar produto', error);
    return Promise.reject(error.message || error);
  }

}
