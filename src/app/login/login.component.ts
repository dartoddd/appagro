import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticateService } from './authenticate.service'
import { Authenticate } from './authenticate'
import { LoaderService } from '../loader.service'


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LogonComponent implements OnInit, OnDestroy {
  sub : any;
  route : string;

  constructor(private authenticationService: AuthenticateService,
              private activateRoute: ActivatedRoute,
              private router: Router,
               private loaderService: LoaderService ) { }



  ngOnInit() {
    this.goto()
    this.sub = this.activateRoute
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        console.info(params)
        this.route = params['route'];
      });
  }


  ngOnDestroy() {
    this.sub.unsubscribe();
  }


  authenticate(email:string , password:string){
    this.loaderService.display(true);
    this.authenticationService.login(email , password)
                              .then( r => this.goto() )
                              .catch(e => this.loaderService.display(false));
    console.info(localStorage.getItem('user-token'))
  }

  goto(){
    if(this.token()){
      this.route = this.route ? this.route : 'produtos'
      this.router.navigate([this.route])
    }
    this.loaderService.display(false);
  }

  logout(){
    this.authenticationService.logout();
  }

  token(){
    return localStorage.getItem('user-token')
  }
}
