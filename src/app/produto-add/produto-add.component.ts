import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import { AuthenticateService } from '../login/authenticate.service';
import { ProdutoAddService } from './produto-add.service';
import { Produto } from '../produtos/produto'
import { LoaderService } from '../loader.service'

@Component({
  selector: 'app-produto-add',
  templateUrl: './produto-add.component.html',
  styleUrls: ['./produto-add.component.css']
})
export class ProdutoAddComponent implements OnInit {
  produto: Produto = new Produto;
  errorMessage: string = null;
  successMessage: string = null;
  title: string = "Adicionar Produto"

  formatos = [
    {value: '1', viewValue: 'Caixa / Pacote'},
    {value: '2', viewValue: 'Rolo / Prisma'},
    {value: '3', viewValue: 'Envolope'}
  ];

  constructor( public authenticate:AuthenticateService,
               private produtoAddService: ProdutoAddService,
               private router: Router,
              public snackBar: MdSnackBar,
               private loaderService: LoaderService  ) { }

  ngOnInit() {
    if (!this.authenticate.token()) {
      this.router.navigate(['login'], { queryParams: { route: 'produto/add' } });
    }
  }

  salvar(){
    if(!this.authenticate.token()){
      console.info(this.authenticate.token()); 
      return this.errorMessage = "Precisa estar logado";
    }
    console.info(this.produto); 

    this.loaderService.display(true);
    
    let response;
    this.produtoAddService.post(this.produto)
                          .then(resp => this.sucesso(resp))
                          .catch(error => this.erro(error._body));
  }

  formatoMudou(){
    console.info("mudou mesmo?")
    this.produto.limpaValores()
  }

  sucesso(resp){
    this.produto = new Produto;
    this.openSnackBar("Inserido com sucesso", "Fechar")
    this.loaderService.display(false);
  }

  erro(erro){
    this.openSnackBar("ERRO" + erro, "Fechar")
    this.loaderService.display(false);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 0,
    });
  }
}
